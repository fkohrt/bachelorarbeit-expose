---
title: Replicating the natural selection of bad science
subtitle: Exposé
author: Florian Kohrt
date: 2021-05-04
lang: en
version: 1.1.2
license: CC0-1.0
---

# Broader context

> The more any quantitative social indicator is used for social decision-making, the more subject it will be to corruption pressures and the more apt it will be to distort and corrupt the social processes it is intended to monitor.
> 
> --- @quotation:Campbell1979 [p. 85].

What @quotation:Campbell1979 formulated with public indicators such as the unemployment index in mind has in the last decade been acknowledged for metrics in academia as well: the transformation into a target which is subject to deliberate attempts of optimization [e.g. @evidence:Bartneck2011; @evidence:Nabout2015]. Examples for these metrics include number of citations, h-index and impact factor [@VanNoorden2010]. There have been suggestions on how to improve them to account for particular drawbacks [@evidence:Panaretos2009], but more recently these metrics have also become subject to a fundamental critique where academia is regarded as a complex social system [@evidence:Edwards2017].

In the field of psychology this specifically includes concerns about the validity of results in the published literature raised by replication failures [@evidence:Laraway2019]. The precedence of positive findings referred to as publication bias has been connected to the current incentive structure of academia where the number of published papers and derived calculations are integral metrics [@evidence:Simmons2011; @evidence:Franco2014]. While the usage of terms such as p-hacking or HARKing may suggest willfull deviations against one's better judgement, another prominent contribution [@evidence:Smaldino2016] notably does not hypothesize about the intentions of individual researchers but only relies on mechanisms of cultural learning to show a decline in methodological rigor. This bachelor's thesis attempts to replicate the findings by @updates:Smaldino2016 -- in the following: "original study" -- and extends their model to explore some of its underlying assumptions.

# Methods and procedure

Following the original study, this thesis uses an agent-based model (ABM) to simulate the actions of individual scientists. An ABM consists of individual agents which are characterized by a micro-level description of attributes and operations and which interactions can lead to the emergence of complex macro-level behaviour. As with all models, constructing ABMs requires abstraction from the details irrelevant to a useful answer [@evidence:Smaldino2017]. For a more thorough introduction, see @recommended_reading:Klein2018.

This thesis will start with a literature review of current software possibilities to implement an ABM, followed by a replication of the original study's _conceptual model_ [@background:Wilensky2007]. Where necessary, the _implemented model_  -- i.e. the source code -- will also be taken into account. The encountered obstacles will be reflected on in a next step, drawing on the recommendations by @background:Hauke2020 concerning the use of the Overview, Design concepts and Details (ODD) protocol [@method:Grimm2020] and the design of experiments (DOE) principles [@method:Lorscheid2012]. Finally, this thesis will explore altering two of the assumptions behind the original model, which are made explicit by @background:Smaldino2019. These assumptions are that "[p]ublishing negative results is difficult or confers little prestige" and that "[p]ublishing positive (confirmatory) results is always possible" [@quotation:Smaldino2019].

# Analyses to be replicated

The original study contains five analyses that need to be replicated:

- the evolution of power in the absence of replications
- the evolution of effort in the absence of replications
- the coevolution of effort and replication for a fixed power
- the evolution of effort with high replication rates
- the distribution of pay-offs for low and high effort labs

This thesis will aim for _relational equivalence_, which is one of the replication standards described by @background:Axelrod1997. This means that both -- the original and the replicated -- implemented models should show qualitatively similar relationships between input and output variables.

# Timeplan

The handling period for this bachelor's thesis will be May 10 -- July 30 2021. The preliminary timeplan is outlined below:

Week | Work
---- | ----
  1  | researching frameworks
  2  | replication
  3  | replication
  4  | replication
  5  | reflecting replication
  6  | creating extension
  7  | creating extension
  8  | writing
  9  | writing
 10  | writing
 11  | proofreading, revision
 12  | typesetting, printing

Table: Preliminary timeplan

# Potential extensions

If time allows, this thesis will also be extended in the following directions:

- adopting the _Software Description Ontology for Models_ [@method:Garijo2021]
- verifying agreement between the original study's conceptual and implemented model as suggested by @background:Zhang2021
- assessing the original study's robustness hypothesis
- implementing models 1 -- 3 as described by @background:Smaldino2019

# License and archival

All materials generated as part of this bachelor's thesis will be made available under the Creative Commons license "[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)". The author aims for archival of the implemented model in the CoMSES repository [@method:Janssen2008; @method:Rollins2014] or a similar institution such as the Software Heritage universal source code archive [@method:DiCosmo2019; @method:DiCosmo2020] that provides permanent access to the supplementary material and a stable identifier.

# Supervision

This thesis will be supervised formally by Prof. Dr. Lynn Huestegge (University of Würzburg), while Prof. Dr. Felix Schönbrodt (Ludwig Maximilian University of Munich) will act as operative supervisor.

